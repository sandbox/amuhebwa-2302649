Feature: Login Devtrac
  In order to login, i have to be on the login page
  Background: In order to login, i have to be on home/index page

  Scenario: Actual login
    Given I am on "/http://jenkinsge.mountbatten.net/devtraccloud/user"
    When I fill in name with "admin"
    And I fill in password with "admin"
    And I press "Login"
    Then I should be redirected to "http://jenkinsge.mountbatten.net/devtraccloud/users/admin"

  Scenario: Create new user
        Given I am logged in as a user with "administer users, administer permissions, access user profiles, administer site configuration, administer modules, create taxonomy_vocabulary_
