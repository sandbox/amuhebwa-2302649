Feature: Edit User Permissions
  In order to to change the permissions, i have to be on user/edit page
  Background: In order to give a user super permissions, i have to be on user/edir page

  Scenario: New User Creation Form
    Given I am logged in as a user with "administer users, administer permissions, access user profiles, administer site configuration, administer modules, create taxonomy_vocabulary_6, creat$
    Given I am on "http://jenkinsge.mountbatten.net/devtraccloud/user/me/edit"
    And I fill in taxonomy_vocabulary_5[und] with "Evaluator"
    And I fill in taxonomy_vocabulary_4[und] with "Education"
    And I set roles[3] to checked
    And I press "Save"
    Then I should be redirected to "http://jenkinsge.mountbatten.net/devtraccloud/admin/people/create"
         
  Scenario: Create New User 
    Given I am logged in as a user with "administer users, administer permissions, access user profiles, administer site configuration, administer modules, create taxonomy_vocabulary_6, creat$
    Given I am on "http://jenkinsge.mountbatten.net/devtraccloud/admin/people/create"
    When I fill in name with "user_demo"
    And I fill in email with "user_demo@mountbatten.net"
    And I fill in pass[pass1] with "user_demo"
    And I fill in pass[pass2] with "user_demo"
    And I fill in taxonomy_vocabulary_5[und] with "Evaluator"
    And I fill in field_user_superior[und][0][target_id] with "AdminTest AdminTest Sir (admin)"
    And I fill in taxonomy_vocabulary_4[und] with "Education"
    And I fill in field_user_firstname[und][0][value] with "User Demo"
    And I fill in field_user_surname[und][0][value] with "User Demo Demo"
    And I fill in field_user_aboutme[und][0][value] with "Iam a hoax user demo test account"
    And I press "Create new account"
    Then I should be redirected to "http://jenkinsge.mountbatten.net/devtraccloud/admin/people/create"
