Feature: Login Devtrac
  In order to login, i have to be on the login page
  Background: In order to login, i have to be on home/index page

  Scenario: Actual login
    Given I am on "/http://jenkinsge.mountbatten.net/devtraccloud/user"
    When I fill in name with "admin"
    And I fill in password with "admin"
    And I press "Login"
    Then I should be redirected to "http://jenkinsge.mountbatten.net/devtraccloud/users/admin"

  Scenario: New User Creation Form
    Given I am logged in as a user with "administer users, administer permissions, access user profiles, administer site configuration, administer modules, create taxonomy_vocabulary_6, creat$
    Given I am on "http://jenkinsge.mountbatten.net/devtraccloud/admin/people/create"
    Then I should see "People"
    Then I should see "Username" field
    Then I should see "E-mail address" field 
    Then I should see "Password" field
    Then I should see "Confirm Password" field
    Then I should see "Function" field
    Then I should see "Manager" field
    Then I should see "Department" field
    Then I should see "First Name" field
    Then I should see "Surname" field
    Then I should see "About Me" field
    Then I should see "Create new account" button

  Scenario: Actual User Creation
    Given I am on "http://jenkinsge.mountbatten.net/devtraccloud/admin/people/create"
    When I fill in name with "user_demo"
    And I fill in email with "user_demo@mountbatten.net"
    And I fill in pass[pass1] with "user_demo"
    And I fill in pass[pass2] with "user_demo"
    And I fill in taxonomy_vocabulary_5[und] with "Evaluator"
    And I fill in field_user_superior[und][0][target_id] with "AdminTest AdminTest Sir (admin)"
    And I fill in taxonomy_vocabulary_4[und] with "Education"
    And I fill in field_user_firstname[und][0][value] with "User Demo"
    And I fill in field_user_surname[und][0][value] with "User Demo Demo"
    And I fill in field_user_aboutme[und][0][value] with "Iam a hoax user demo test account"
    And I press "Create new account"
    Then I should be redirected to "http://jenkinsge.mountbatten.net/devtraccloud/admin/people/create"
         
